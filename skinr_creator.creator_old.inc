<?php

/**
 * This is the main form that skin creator is using.
 */
function skinr_creator_old_form ($form, &$form_state, $generated = NULL) {
  $form = array();

	if (!isset($form_state['stored']['groups']) || !isset($form_state['stored']['skins'])) {
		$form_state['stored']['groups']['count'] = 1;
		$form_state['stored']['skins']['count'] = 1;
	}
	
	$form['tabs'] = array(
		'#type' => 'vertical_tabs',
	);
		
  $form['theme_hook'] = array(
		'#type' => 'fieldset',
		'#title' => t('Theme Hook'),
  	'#group' => 'tabs',
	);

  // Lists installed themes.
  $themes = list_themes();
  $options = array();

  foreach ($themes as $theme) {
    if (!empty($theme->info['hidden'])) {
      continue;
    }
    if ($theme->status) {
      $options[$theme->name] = t('!theme [enabled]', array('!theme' => $theme->info['name']));
    }
    else {
      $options[$theme->name] = $theme->info['name'];
    }
  }

  // Function elements.
  $form['theme_hook']['source_name'] = array(
    '#type' => 'select',
    '#title' => t('Theme'),
    '#description' => t('Select the theme to create a skin for.'),
    '#options' => $options,
    '#required' => TRUE,
    '#default_value' => 'Bartik',
  );

  $form['theme_hook']['plugin_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Plugin name'),
    '#description' => t('Enter a name for this plugin. ( a-z, 0-9, _ )'),
    '#required' => TRUE,
    '#default_value' => 'wires',
  );

  // Group elements.
	// @todo add foreach
  $form['groups'] = array(
		'#tree' => TRUE,
    '#type' => 'fieldset',
    '#title' => t('Groups'),
		'#group' => 'tabs',
  );
	for ($i = 0; $i < $form_state['stored']['groups']['count']; $i++) {
	  $form['groups'][$i]['group'] = array(
			'#tree' => TRUE,
	    '#type' => 'fieldset',
	    '#title' => t('Group !count', array('!count' => $i+1)),
	    '#collapsible' => TRUE,
	    '#collapsed' => FALSE,
	  );
	  $form['groups'][$i]['group']['group_title'] = array(
	    '#type' => 'textfield',
	    '#title' => t('Title'),
	    '#description' => t('Enter a title for the group.'),
	    '#required' => FALSE,
	    '#default_value' => 'groups', 
	  );
	  $form['groups'][$i]['group']['group_name'] = array(
	    '#type' => 'textfield',
	    '#title' => t('Name'),
	    '#description' => t('Enter a machine_name for the group. ( a-z, 0-9, _ )'),
	    '#required' => FALSE,
	    '#default_value' => 'groups', 
	  );
	  $form['groups'][$i]['group']['group_description'] = array(
	    '#type' => 'textfield',
	    '#title' => t('Description'),
	    '#description' => t('Enter a description for this group.'),
	    '#required' => TRUE,
	    '#default_value' => 'test', 
	  );
	  $form['groups'][$i]['group']['group_weight'] = array(
	    '#type' => 'textfield',
	    '#title' => t('Weight'),
	    '#description' => t('Enter a WEIGHT for this group.'),
	    '#required' => TRUE,
	    '#default_value' => '0', 
  	);
	}
	$form['groups']['add_more'] = array(
	  '#type' => 'button',
	  '#value' => t('Add'),
	);
	
  // Skin elements.
	// @todo add foreach
  $form['skins'] = array(
    '#type' => 'fieldset',
    '#title' => t('Skins'),
		'#group' => 'tabs',
  );

  $form['skins']['status'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Status'),
    '#description' => t('This property exists to allow themes to set the default status for a particular theme. !variable.', array('!variable' => l(t('More info'), 'http://skinr.org/skin-property-reference#property-status', array('attributes' => array('target' => '_blank'))))),
    '#options' => $options,
    '#required' => TRUE,
    '#default_value' => array('bartik'),   
  );

  $form['skins']['group'] = array(
	  '#type' => 'select',
	  '#title' => t('Group'),
	  '#options' => array(
	    'general' => 'general',
	    'box' => 'box',
	    'typography' => 'typography',
			'layout' => 'layout',
			'custom' => 'custom',
	  ),
	  '#description' => t('Global setting for the length of XML feed items that are output by default.'),
    '#default_value' => 'general',   
	);
	$form['skins']['group_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Group name'),
    '#description' => t('Enter the name for the group.'),
    '#required' => FALSE,
    '#default_value' => 'default', 
  );
	
  $form['skins']['skin_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Skin name'),
    '#description' => t('Enter a machine_name for the skin. ( a-z, 0-9, _ )'),
    '#required' => TRUE,
    '#default_value' => 'wires', 
  );

  $form['skins']['skin_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('The title of the skin. !variable.', array('!variable' => l(t('More info'), 'http://skinr.org/skin-property-reference#property-title', array('attributes' => array('target' => '_blank'))))),
    '#required' => TRUE,
    '#default_value' => 'wires',
  );

  $form['skins']['skin_description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#description' => t('The form element description. !variable.', array('!variable' => l(t('More info'), 'http://skinr.org/skin-property-reference#property-description', array('attributes' => array('target' => '_blank'))))),
    '#required' => TRUE,
    '#default_value' => 'Adds wireframes to the skin.',
  );

  $form['skins']['skin_type'] = array(
    '#type' => 'select',
    '#title' => t('Type'),
    '#options' => array('checkboxes' => 'checkboxes', 'radios' => 'radios', 'select' => 'select'),
    '#description' => t('The type of form element to use when displaying the options to the user in the settings form. !variable.', array('!variable' => l(t('More info'), 'http://skinr.org/skin-property-reference#property-type', array('attributes' => array('target' => '_blank'))))),
    '#required' => TRUE,
    '#default_value' => 'select',
    );

  //Lists the default theme hooks as well as user defined theme hooks.
  $items = explode("\n", variable_get('skinr_creator_add_theme_hooks', SKINR_CREATOR_DEFAULT_THEME_HOOKS));
  $items = array_map('trim', $items);
  $items = array_filter($items, 'strlen');

  $options = array();
  foreach ($items as $item) {
    $options[$item] = $item;
  }

  $form['skins']['skin_select_hooks'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Theme hooks'),
    '#description' => t('The theme_hooks this skin will apply to. !variable.', array('!variable' => l(t('Add theme_hooks'), 'admin/appearance/skinr/skinr_creator_settings'))),
    '#options' => $options,
    '#required' => TRUE,
    '#default_value' => array('block'),
  );

  $form['skins']['skin_options_num'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of options:'),
    '#description' => t('The number of options for this skin.'),
    '#required' => TRUE,
    '#default_value' => '3',
  );

  $form['skins']['skin_options_machine_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Machine name:'),
    '#description' => t('This name will iterate according to the number of classes.'),
    '#required' => TRUE,
    '#default_value' => 'Wires',
  );

  $form['skins']['skin_css'] = array(
    '#type' => 'textfield',
    '#title' => t('CSS File'),
    '#description' => t('This field is where you define your CSS file.'),
    '#default_value' => 'mycss.css',  
  );
  
  $form['skins']['skin_js'] = array(
    '#type' => 'textfield',
    '#title' => t('JS File'),
    '#description' => t('This field is where you define your Javascript file.'),
  );

  $form['submit'] = array('#type' => 'submit', '#value' => t('Create'));

  // Prevent browser caching by adding the timestamp to the field name.
  $form['generated' . time()] = array(
    '#type' => 'textarea',
    '#default_value' => isset($form_state['generated']) ? $form_state['generated'] : '',
    '#rows' => min(isset($form_state['generated']) ? substr_count($form_state['generated'], "\n") + 1 : 10, 150),
    '#attributes' => array('readonly' => 'readonly'),
  );
  
	 // Vertical tabs.
	 // Replicate the entire form; some more black magic.
	 $form['group'] = array(
	'#type' => 'vertical_tabs',
	);
	$form['tabs'][0] = array(
	'#type' => 'fieldset',
	'#title' => 'Vertical tab 1',
	'#description' => 'Description',
	'#group' => 'group',
	);

	$form['tabs'][1] = array(
	'#type' => 'fieldset',
	'#title' => 'Vertical tab 2',
	'#description' => '<p>Description</p><p>Description</p>',
	'#group' => 'group',
	);

	// In case you didn't know, vertical tabs are supported recursively.
	$form['tabs'][0]['subgroup'] = array(
	'#type' => 'vertical_tabs',
	);

	$form['subtabs'][0] = array(
	'#type' => 'fieldset',
	'#title' => 'Vertical tab 1',
	'#description' => 'Description',
	'#group' => 'subgroup',
	);

	$form['subtabs'][1] = array(
	'#type' => 'fieldset',
	'#title' => 'Vertical tab 2',
	'#description' => '<p>Description</p><p>Description</p>',
	'#group' => 'subgroup',
	);

	return $form;
}

/**
 * This function adds validation to the form elements.
 */
function skinr_creator_form_validate($form, &$form_state) {
  // Invalid Plugin Name.
  if (!preg_match('!^[a-z0-9_]+$!', $form_state['values']['plugin_name'])) {
    form_set_error('plugin_name', t('<em>Plugin name:</em> The name must include only lowercase unaccentuated letters, numbers, and underscores.'));
  }
  if (strlen($form_state['values']['plugin_name']) > 32) {
    form_set_error('plugin_name', t('<em>Plugin name:</em> the plugin name %field_name is too long. The name is limited to 32 characters.', array('%field_name' => $form_state['values']['plugin_name'])));
  }

  // Invalid Skin Name.
  if (!preg_match('!^[a-z0-9_]+$!', $form_state['values']['skin_name'])) {
    form_set_error('skin_name', t('<em>Plugin name:</em> The name must include only lowercase unaccentuated letters, numbers, and underscores.'));
  }
  if (strlen($form_state['values']['skin_name']) > 32) {
    form_set_error('skin_name', t('<em>Skin name:</em> the skin name %field_name is too long. The name is limited to 32 characters.', array('%field_name' => $form_state['values']['plugin_skin'])));
  }  
    
  // Invalid Number of options.  
  if (!is_numeric($form_state['values']['skin_options_num'])) {
    form_set_error('skin_options_num', t('<em>Number of options:</em> must be a valid number'));
  }
}

/**
 * This function adds the submit button and adds the generated contents 
 */
function skinr_creator_form_submit(&$form, &$form_state) {
//dpm($form_state['values']['groups']);

  // If a theme is checkmarked assign it a value of 1 
  $status = array_filter($form_state['values']['status']);
  if (!empty($status)) {
    foreach ($status as $key => $value) {
      $status[$key] = 1;
    }
  }

  // All themes that have a value of 0 in the array will be removed from the array.
  $theme_hooks = _skinr_array_strip_empty($form_state['values']['skin_select_hooks']);

  // Skin variable is defined here and then later passed into the function skinr_creator_var_export.
  $skin = array(
    'status' => $status,
    'title' => t($form_state['values']['skin_title']),
    'description' => t($form_state['values']['skin_description']),
    'type' => $form_state['values']['skin_type'],
    'theme hooks' => array_keys($theme_hooks),
    
    // @todo Needs: Adding of group feature.
    
  );
  
  // Adds CSS and JS attached at the Skin level.
  if (!empty($form_state['values']['skin_css']) || !empty($form_state['values']['skin_js'])) {
    $skin['attached'] = array();  
    if (!empty($form_state['values']['skin_css'])) {
      $skin['attached']['css'] = array($form_state['values']['skin_css']);
    }
    if (!empty($form_state['values']['skin_js'])) {
      $skin['attached']['js'] = array($form_state['values']['skin_js']);      
    }
  }
  
  // This variable is used to change underscores to dashes for CSS Compatiblity.
  $clean_machine_name = str_replace('_', '-', $form_state['values']['skin_options_machine_name']);

  // Skin options iterated here according to the user inputted number.
  $skin['options'] = array();  
  for ($i = 1; $i <= $form_state['values']['skin_options_num']; $i++) {
    $skin['options'][$form_state['values']['skin_options_machine_name'] . '_' . $i] = array(
      'class' => array($clean_machine_name . '-' . $i),
      'title' => t('Please modify this human readable info for ' . $form_state['values']['skin_options_machine_name'] . '-' . $i),
    );
  }

  // Opening skin info concatenated with var_exported skin.
  $generated_skin = skinr_creator_var_export($skin, '  ');

	$groups = '';
	foreach ($form_state['values']['groups'] as $form_group) {
		if (!is_array($form_group)) {
			continue;
		}
		$group['title'] = $form_group['group']['group_title'];
		$group['description'] = $form_group['group']['group_description'];
		$group['weight'] = $form_group['group']['group_weight'];

		$groups .= "  \$groups['{$form_group['group']['group_name']}'] = " . skinr_creator_var_export($group, '  ');
	}


	$groups_ = <<<END
/**
 * Implements hook_skinr_group_plugin_info().
 */
function {$form_state['values']['source_name']}_skinr_group_{$form_state['values']['source_name']}_info() {
$groups;
  return \$groups;
}
END;

  // Opening info for a skin.
  $generated = <<<END
<?php

/**
 * @file
 * MODIFY THIS INFO TO DESCRIBE THE FILE.
 */

$groups_

/**
 * Implements hook_skinr_skin_plugin_info().
 */
function {$form_state['values']['source_name']}_skinr_skin_{$form_state['values']['plugin_name']}_info() {
  \$skins['{$form_state['values']['skin_name']}'] = $generated_skin;
return \$skins;
}
END;

  // Pass generated back to the textarea.
  $form_state['generated'] = $generated;
  $form_state['rebuild'] = TRUE;
  $form_state['cache'] = FALSE;
	$form_state['stored']['skins']['count']++;
}