<?php

/**
 * Implements hook_help().
 */
function skinr_creator_help($path, $arg) {
  switch ($path) {
    case 'admin/appearance/skinr/skinr_creator_converter':
      return t('Use this page to convert and export a skin to a compressed tar file. Uncompress the file to your theme in folder called skins.', array('@skinr-help' => url('admin/advanced_help/skinr')));
      break;
  }
}

/**
 * Read in the skins from a folder. 
 */
function skinr_converter_form($form) {
  // $infos runs ...parse_info_files function and generates an array for each
  // theme.  key = theme_name & value = array(theme_info)
  $infos = skinr_converter_parse_info_files();

  // $options is an array whose values come from looping through the $infos
  // and setting $options' keys to the theme_names and its values name.
  $options = array();  
  foreach($infos as $name => $info){
    if(isset($info['skinr'])){
      $options[$name] = $info['name'];
    }
  }
  asort($options);

  $form = array();
  $form['files'] = array(
    '#type' => 'radios',
    '#title' => t('Theme'),
    '#description' => t('Select the theme to create a skin for.  Only .info files with valid skin configurations will be listed.'),
    '#options' => $options,
    '#required' => TRUE,
  );
  $form['submit'] = array('#type' => 'submit', '#value' => t('Convert'));

  return $form;
}

/**
 * Parse .info files.
 */
function skinr_converter_parse_info_files() {
  // This variable calls a system function that reads in the .info files.
  // $files is an array whose values are classes containing URI, filename, and
  // the name of the .info files
  $files = drupal_system_listing("/\.info$/", DRUPAL_ROOT . '/' . variable_get('skin_creator_path', SKINR_CONVERTER_DEFAULT_PATH), 'name', 0);

  $info = array();
  foreach($files as $name => $file) {
    $info[$name] = drupal_parse_info_file($file->uri);
    $info[$name]['filepath'] = $file->uri;
  }
  // $info is an array whose keys are the machine names of the themes and the
  // values are arrays from the parse containing the info of each theme.
  return $info;
}

/**
 * Processes to be run after convert is clicked.  Takes parsed files and packs
 * them in .tar file.
 */
function skinr_converter_form_submit($form, &$form_state, $converted = NULL) {
  // $infos is an array whose keys are the machine names of the themes and the 
  // values are arrays from the parse containing the info of each theme.
  $infos = skinr_converter_parse_info_files();
  
  if (!empty($form_state['values']['files'])) {
    $theme_name = $form_state['values']['files'];
    $data = skinr_converter_convert_info($theme_name, $infos[$theme_name]['skinr']);
    if ($data !== FALSE) {
      $files = skinr_converter_get_files($data['skins']);
      $converted = skinr_converter_skin_plugin_generate($theme_name, $data['groups'], $data['skins']);
      skinr_creator_make_file($theme_name, $converted, $files);
    }
  }
$form_state['rebuild'] = TRUE;
}

/**
 * Traverses arrays to find the attached files for JS and CSS.
 */
function skinr_converter_get_files($skins) {
  $files = array();

  foreach ($skins as $skin) {
    if (!empty($skin['attached'])) {
      foreach ($skin['attached'] as $type => $entries) {
        if ($type == 'css' || $type == 'js') {
          foreach ($entries as $entry) {
            if (is_array($entry)) {
              $files[] = $entry[0];
            }
            else {
              $files[] = $entry;
            }
          }
        }
      }
    }
    foreach ($skin['options'] as $option) {
      if (!empty($option['attached'])) {
        foreach ($option['attached'] as $type => $entries) {
          if ($type == 'css' || $type == 'js') {
            foreach ($entries as $entry) {
              if (is_array($entry)) {
                $files[] = $entry[0];
              }
              else {
                $files[] = $entry;
              }
            }
          }
        }
      }
    }
  }

  $files = array_unique($files);
  return $files;
}

/**
 * Convert the array from the 6.x .info file to a php skin.
 */
function skinr_converter_convert_info($theme_name, $info) {
  $error = FALSE;
  $skins = array();
  $groups = array();

  foreach ($info as $skin_name => $old_skin) {

    if (!is_array($old_skin)) {
      continue;
    }

    if ($skin_name == 'options'){
			if (!empty($old_skin['groups'])) {
	      foreach ($old_skin['groups'] as $group_name => $old_group) {
	        $group = array(
	          'title' => $old_group['title'],
	          'description' => 'Please enter a description for this group.',
	        );
	        $groups[$group_name] = $group;
	      }
			}
      continue;
    }

    $skin = array();

    if (empty($old_skin['title'])) {
      $error = TRUE;
      drupal_set_message(t('Title for skin %skin_name is required.', array('%skin_name' => $skin_name)), 'warning');
    }
    else {
      $skin['title'] = $old_skin['title'];
    }
  
    if (!empty($old_skin['description'])) {
      $skin['description'] = $old_skin['description'];
    }
    
    if (!empty($old_skin['select'])){
      $skin['type'] = $old_skin['select'];
    }
    
    if (!empty($old_skin['features'])){
      $skin['theme hooks'] = $old_skin['features'];
    }
    
    if (!empty($old_skin['group'])){
      $skin['group'] = $old_skin['group'];
    }

    if (!empty($old_skin['stylesheets']) || !empty($old_skin['scripts'])) {
      $attached = array();
      if (!empty($old_skin['stylesheets'])) {
        $attached['css'] = array();
        foreach ($old_skin['stylesheets'] as $css_key => $paths) {
          if ($css_key == 'all') {
            $attached['css'] += $old_skin['stylesheets'][$css_key];
          }
          else {
            foreach ($paths as $path) {
              $attached['css'][] = array($path, array('media' => $css_key));
            }
          }
        }
      }
      // This is working but not for the proper level
      if (!empty($old_skin['scripts'])) {
        $attached['js'] = $old_skin['scripts'];
      }
      $skin['attached'] = $attached;
    }

    $skin['default status'] = 0;
    $skin['status'] = array($theme_name => 1);
    
    if (!empty($old_skin['weight'])) {
      $skin['weight'] = $old_skin['weight'];
    }
    
    if (empty($old_skin['options'])){
      $error = TRUE;
      drupal_set_message(t('Options for skin %skin_name are required.', array('%skin_name' => $skin_name)), 'warning');      
    } 
    else {
      $skin['options'] = array();
      
      foreach ($old_skin['options'] as $option_key => $option) {
        $skin['options'][$skin_name . '_' . $option_key] = array(
          'title' => $option['label'],
          'class' => $option['class'],
        );

        if (!empty($option['stylesheets']) || !empty($option['scripts'])) {
          $attached = array();
          if (!empty($option['stylesheets'])) {
            $attached['css'] = array();
            foreach ($option['stylesheets'] as $css_key => $paths) {
              if ($css_key == 'all') {
                $attached['css'] += $option['stylesheets'][$css_key];
              }
              else {
                foreach ($paths as $path) {
                  $attached['css'][] = array($path, array('media' => $css_key));
                }
              }
            }
          }
          if (!empty($option['scripts'])) {
            $attached['js'] = $option['scripts'];
          }
          $skin['options'][$skin_name . '_' . $option_key]['attached'] = $attached;
        }
      }    
    }
    $skins[$skin_name] = $skin;
  }

  if ($error) { 
    return FALSE;
  }
  else {
    return array('groups' => $groups, 'skins' => $skins);
  }
}

/**
 * Adds the opening PHP tag, DOXYGEN Info, hook info and runs skinr_var_export
 * creating a properly formatted string.
 */
function skinr_converter_skin_plugin_generate($theme_name, $groups, $skins) {
  $group_output = '';
  foreach ($groups as $name => $group) {
    $group_output .= "  \$groups['{$name}'] = " . skinr_creator_var_export($group, '  ') . ";\n\n";
  }

  $skin_output = '';
  foreach ($skins as $name => $skin) {
    $skin_output .= "  \$skins['{$name}'] = " . skinr_creator_var_export($skin, '  ') . ";\n\n";
  }

  $converted = <<<END
<?php

/**
* @file
* MODIFY THIS INFO TO DESCRIBE THE FILE.
*/

/**
* Implements hook_skinr_group_plugin_info().
*/
function {$theme_name}_skinr_group_{$theme_name}_info() {
{$group_output}  return \$groups;
}

/**
* Implements hook_skinr_skin_plugin_info().
*/
function {$theme_name}_skinr_skin_{$theme_name}_info() {
{$skin_output}  return \$skins;
}
END;

  return $converted;
}