<?php
/**
 * This is the code for the settings page for Skin Creator.
 */
function skinr_creator_settings_form() {
  $form = array();

  $form['skinr_creator_add_theme_hooks'] = array(
    '#type' => 'textarea',
    '#title' => 'Theme hooks',
    '#default_value' => variable_get('skinr_creator_add_theme_hooks', SKINR_CREATOR_DEFAULT_THEME_HOOKS),
    '#description' => 'This field contains theme hooks. Enter one value per line.',
  );

  $form['skinr_creator_converter_add_theme_hooks'] = array(
    '#type' => 'textfield',
    '#title' => 'Conversion path',
    '#default_value' => variable_get('skinr_creator_converter_add_theme_hooks', SKINR_CONVERTER_DEFAULT_PATH),
    '#description' => 'A local file system path where 6.x files will be placed. This directory must exist and be writable by Drupal. This directory must be relative to the Drupal installation directory.',
  );

  return system_settings_form($form);
}