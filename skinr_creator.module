<?php
// $Id$

/**
 * @file
 * This is the main nodule file for Skin Creator.
 */
define('SKINR_CREATOR_DEFAULT_THEME_HOOKS', "page\nnode\nregion\nblock\ncomment_wrapper\nviews_view\npanels_display\npanels_region\npanels_pane");
define('SKINR_CONVERTER_DEFAULT_PATH', 'sites/all/files/skin_conversion/old_skins');

/**
 * Implementation of hook_menu().
 */
function skinr_creator_menu() {
  // List installed and cached plugins 
  $items['admin/appearance/skinr/plugin'] = array(
    'title' => 'Plugins',
    'access arguments' => array('administer skinr'),
    'page callback' => 'skinr_creator_plugin_list',
    'type' => MENU_LOCAL_TASK,
    'weight' => '3',
    'file' => 'skinr_creator.creator.inc',
  );

  // Add plugin.
  $items['admin/appearance/skinr/plugin/add'] = array(
    'title' => 'Add plugin',
    'access arguments' => array('administer skinr'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('skinr_creator_plugin_add'),
    'type' => MENU_LOCAL_ACTION,
    'file' => 'skinr_creator.creator.inc',
  );
  // Edit plugin.
  $items['admin/appearance/skinr/plugin/%/%/edit'] = array(
    'title' => 'Edit plugin',
    'access arguments' => array('administer skinr'),
    'page callback' => 'skinr_creator_plugin_edit',
    'page arguments' => array(4, 5),
    'file' => 'skinr_creator.creator.inc',
  );

  // Add group.
  $items['admin/appearance/skinr/plugin/%/%/group/add'] = array(
    'title' => 'Add group',
    'access arguments' => array('administer skinr'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('skinr_creator_plugin_group_add', 4, 5),
    'file' => 'skinr_creator.creator.inc',
  );

  // Edit group.
  $items['admin/appearance/skinr/plugin/%/%/group/%'] = array(
    'title' => 'Edit group',
    'access arguments' => array('administer skinr'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('skinr_creator_plugin_group_edit', 4, 5, 7),
    'file' => 'skinr_creator.creator.inc',
  );
  // Delete group
  $items['admin/appearance/skinr/plugin/%/%/group/%/delete'] = array(
    'title' => 'Delete group',
    'access arguments' => array('administer skinr'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('skinr_creator_plugin_group_delete_confirm', 4, 5, 7),
    'file' => 'skinr_creator.creator.inc',
  );

  // Add skin.
  $items['admin/appearance/skinr/plugin/%/%/skin/add'] = array(
    'title' => 'Add skin',
    'access arguments' => array('administer skinr'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('skinr_creator_plugin_skin_add', 4, 5, 7),
    'file' => 'skinr_creator.creator.inc',
  );
  // Edit skin.
  $items['admin/appearance/skinr/plugin/%/%/skin/%'] = array(
    'title' => 'Edit skin',
    'access arguments' => array('administer skinr'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('skinr_creator_plugin_skin_edit', 4, 5, 7),
    'file' => 'skinr_creator.creator.inc',
  );
  // Delete skin.
  $items['admin/appearance/skinr/plugin/%/%/skin/%/delete'] = array(
    'title' => 'Delete skin',
    'access arguments' => array('administer skinr'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('skinr_creator_plugin_skin_delete_confirm', 4, 5, 7),
    'file' => 'skinr_creator.creator.inc',
  );

  // Settings page for Skin Creator.
  $items['admin/appearance/skinr/skinr_creator_settings'] = array(
    'title' => 'Skin Creator Settings',
    'description' => 'Use to configure settings for Skinr',
    'access arguments' => array('administer skinr'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('skinr_creator_settings_form'),
    'type' => MENU_LOCAL_TASK,
    'weight' => '4',
    'file' => 'skinr_creator.admin.inc',
  );

  // Skin Converter Page.
  $items['admin/appearance/skinr/skinr_creator_converter'] = array(
    'title' => 'Skin Converter',
    'description' => 'Use to convert Skins',
    'access arguments' => array('administer skinr'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('skinr_converter_form'),
    'type' => MENU_LOCAL_TASK,
    'weight' => '5',
    'file' => 'skinr_creator.converter.inc',
  );
  
  return $items;
}

/**
 * Export a variable.
 *
 * This is a replacement for var_export(), allowing us to more nicely
 * format exports. It will recurse down into arrays and will try to
 * properly export bools when it can, though PHP has a hard time with
 * this since they often end up as strings or ints.
 */
function skinr_creator_var_export($var, $prefix = '', $force_newline = FALSE) {

  if (is_array($var)) {
    if (empty($var)) {
      $output = 'array()';
    }
    else {
      $numkeyed = is_numeric(key($var));
      // This only appears in attached.
      $inline = (key($var) == 'media');
      if (!$force_newline && ($numkeyed || $inline)) {
        $output = 'array(';
      }
      else {
        $output = "array(\n";
      }
      $counter = 0;
      foreach ($var as $key => $value) {
        if (in_array($key, array('title', 'description')) && $key !== 0) {

          $output .= $prefix . "  " . skinr_creator_var_export($key) . " => t(" . skinr_creator_var_export($value, $prefix . '  ') . "),\n";
        }
        elseif ($numkeyed && $force_newline) {
          $output .= $prefix . "  " . skinr_creator_var_export($value) . ",\n";
        }
        elseif ($numkeyed) {
          $output .= ($key == 0 ? '' : ', ') . skinr_creator_var_export($value);
        }
        elseif ($inline) {
          $output .= ($counter == 0 ? '' : ', ') . skinr_creator_var_export($key) . " => " . skinr_creator_var_export($value);
        }
        else {
          $next_newline =  (in_array($key, array('css', 'js')) && is_array($value) && count($value) > 1);
          $output .= $prefix . "  " . skinr_creator_var_export($key) . " => " . skinr_creator_var_export($value, $prefix . '  ', $next_newline) . ",\n";
        }
        $counter++;
      }
      if (!$force_newline && ($numkeyed || $inline)) {
        $output .= ')';
      }
      else {
        $output .= $prefix . ')';
      }
    }
  }
  elseif (is_object($var) && get_class($var) === 'stdClass') {
    // var_export() will export stdClass objects using an undefined
    // magic method __set_state() leaving the export broken. This
    // workaround avoids this by casting the object as an array for
    // export and casting it back to an object when evaluated.
    $output .= '(object) ' . skinr_creator_var_export((array) $var);
  }
  elseif (is_bool($var)) {
    $output = $var ? 'TRUE' : 'FALSE';
  }
  else {
    $output = var_export($var, TRUE);
  }

  return $output;
}

/**
 * This function is used to gather the files that will be passed to the
 * function skinr_creator_tar_create.
 */
function skinr_creator_make_file($theme_name, $skins, $files){
  if (!empty($skins)) {
    // Clear out output buffer to remove any garbage from tar output.
    if (ob_get_level()) {
      ob_end_clean();
    }

    $infos = skinr_converter_parse_info_files();
    $theme_path = dirname($infos[$theme_name]['filepath']);

    drupal_add_http_header('Content-type', 'application/x-tar');
    drupal_add_http_header('Content-Disposition', 'attachment; filename="'. $theme_name . '.tar' .'"');
    drupal_send_headers();

    $extension = 'inc';
    $path = $theme_name;
    print skinr_creator_tar_create("$path/$theme_name.$extension", $skins);

    foreach ($files as $file) {
      $contents = @file_get_contents($theme_path . '/' . $file);
        if ($contents === FALSE) {
          drupal_set_message(t('This is a problem file: %file', array('%file' => $file)), 'warning');
        }
        if (empty($contents)) {
          $contents = "/*\n * This file was empty or did not exist.\n */\n";
        }
      print skinr_creator_tar_create("$path/$file", $contents);
      unset($contents);
    }

    // Adds 1024 byte footer at the end of the tar file.
    print pack("a1024","");
  }
}

/**
 * Tar creation function. Written by dmitrig01.
 *
 * @param $name
 *   Filename of the file to be tarred.
 * @param $contents
 *   String contents of the file.
 *
 * @return
 *   A string of the tar file contents.
 *
 * @todo If $contents is an empty string, the tar file will not be generated
 *   properly.
 */
function skinr_creator_tar_create($name, $contents) {
  $tar = '';
  $binary_data_first = pack("a100a8a8a8a12A12",
    $name,
    '100644 ', // File permissions
    '   765 ', // UID,
    '   765 ', // GID,
    sprintf("%11s ", decoct(strlen($contents))), // Filesize,
    sprintf("%11s", decoct(REQUEST_TIME)) // Creation time
  );
  $binary_data_last = pack("a1a100a6a2a32a32a8a8a155a12", '', '', 'ustar ', ' ', '', '', '', '', '', '');

  $checksum = 0;
  for ($i = 0; $i < 148; $i++) {
    $checksum += ord(substr($binary_data_first, $i, 1));
  }
  for ($i = 148; $i < 156; $i++) {
    $checksum += ord(' ');
  }
  for ($i = 156, $j = 0; $i < 512; $i++, $j++) {
    $checksum += ord(substr($binary_data_last, $j, 1));
  }

  $tar .= $binary_data_first;
  $tar .= pack("a8", sprintf("%06s ", decoct($checksum)));
  $tar .= $binary_data_last;

  $buffer = str_split($contents, 512);
  foreach ($buffer as $item) {
    $tar .= pack("a512", $item);
  }
  return $tar;
}
